package com.furymaxim.bannermiddle

import android.content.res.Resources
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.ImageView

import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.furymaxim.bannermiddle.MyScrollView.OnScrollStoppedListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var repeat = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        scrollView.setOnTouchListener(OnTouchListener { v, event ->

            if(!repeat) {
                if (event.action == MotionEvent.ACTION_UP) {

                    scrollView.startScrollerTask()
                }
            }
            false
        })


        scrollView.setOnScrollStoppedListener(OnScrollStoppedListener {

            if(!repeat) {
                Handler().postDelayed({
                    Toast.makeText(applicationContext, "stopped for 1 sec", Toast.LENGTH_SHORT)
                        .show()

                    val scrollBounds = Rect()

                    scrollView.getHitRect(scrollBounds)


                    val top = scrollBounds.top
                    val bottom = scrollBounds.bottom

                    val middle = (top + bottom) / 2  // мы определили позицию центра


                    // ну и как смотреть какие строчки у нас visible, если только для каждой строчки генерировать
                    // TextView и потом смотреть visible или нет

                    // ccылка https://stackoverflow.com/questions/4628800/android-how-to-check-if-a-view-inside-of-scrollview-is-visible


                    Log.d("myLogs", middle.toString())

                    val lines: MutableList<CharSequence> = ArrayList()  // здесь у нас хранятся линии

                    val count: Int = textView.getLineCount()
                    for (line in 0 until count) {
                        val start: Int = textView.getLayout().getLineStart(line)
                        val end: Int = textView.getLayout().getLineEnd(line)
                        val substring: CharSequence = textView.getText().subSequence(start, end)
                        lines.add(substring)

                        //11 lines
                    }

                    // первая часть текста

                    val sbStart = StringBuilder()

                    for ((index, value) in lines.withIndex()) {
                        if (index <= 17) {
                            sbStart.append(value)
                        }
                    }


                    // вторая часть текста


                    val sbEnd = StringBuilder()

                    for ((index, value) in lines.withIndex()) {
                        if (index > 17) {
                            sbEnd.append(value)
                        }
                    }


                    textView.visibility = View.GONE


                    val sbArray = listOf(sbStart, sbEnd)

                    val textView1 = TextView(this)

                    textView1.id = System.currentTimeMillis().toInt()
                    textView1.setText(sbArray[0])
                    textView1.setTextColor(getColor(R.color.colorAccent))


                 /*   val recentViewText = textView1


                    var params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )


                  */

                    //params.addRule(LinearLayout., recentViewText.id)

                    val imageView = ImageView(this)

                    imageView.setPadding(5, 5, 5, 5)
                    imageView.setImageDrawable(getDrawable(R.drawable.image))

                    val textView2 = TextView(this)

                    textView2.id = System.currentTimeMillis().toInt()
                    textView2.setText(sbArray[1])
                    textView2.setTextColor(getColor(R.color.colorPrimary))

                    myLinearLayout.addView(textView1)
                    myLinearLayout.addView(imageView)
                    myLinearLayout.addView(textView2)


                }, 1000)

                repeat = true
            }
        })


    }

    fun pxToDp(px: Int): Int {
        return (px / Resources.getSystem().getDisplayMetrics().density).toInt()
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().getDisplayMetrics().density).toInt()
    }
}
